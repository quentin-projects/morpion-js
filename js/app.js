// solutions ::
// 1 => A1,A2,A3
// 2 => B1,B2,B3
// 3 => C1,C2,C3
// 4 => A1,B1,C1
// 5 => A2,B2,C2
// 6 => A3,B3,C3
// 7 => A1,B2,C3
// 8 => A3,B2,C1

var solutions = new Array();
solutions['a1'] = [1,4,7]
solutions['a2'] = [1,5]
solutions['a3'] = [1,6,8]
solutions['b1'] = [2,4]
solutions['b2'] = [2,5,7,8]
solutions['b3'] = [2,6]
solutions['c1'] = [3,4,8]
solutions['c2'] = [3,5]
solutions['c3'] = [3,6,7]
var joueur_croix = new Array()
var joueur_rond = new Array()
var joueur = 'croix';
var winner = 0;

function init() {
    for (var i = 1; i <= 8 ; i++) {
        joueur_croix[i] = 0;
        joueur_rond[i] = 0;
    }
}
function play(el) {
    if(joueur == 'croix') { 
        tour(el, 'croix', joueur_croix, 'rond');
    } else if (joueur == 'rond') {
        tour(el, 'rond', joueur_rond, 'croix');
    }
}

function tour(el, joueur_courant, tableau_courant, next_joueur) {
    solutions[el.id].forEach(function(element) {
        tableau_courant[element] = tableau_courant[element] + 1;
        el.className = joueur_courant;
        document.getElementById('joueur').innerHTML = next_joueur;
        if (tableau_courant[element] == 3) {
            winner = joueur_courant;
            document.getElementById('winner').innerHTML = 'Joueur ' + joueur_courant + ' gagne !';
        }
        joueur = next_joueur;
    });
}

function reset() {
    document.getElementById('a1').className = document.getElementById('a2').className = document.getElementById('a3').className = 'case';
    document.getElementById('b1').className = document.getElementById('b2').className = document.getElementById('b3').className = 'case';
    document.getElementById('c1').className = document.getElementById('c2').className = document.getElementById('c3').className = 'case';
    document.getElementById('winner').innerHTML = '';
    joueur_croix = new Array();
    joueur_rond = new Array();
    winner = 0;
    init();
}

window.onload = function() {
   var cases = document.getElementsByClassName('case');
   for(var i = 0; i < cases.length; i++) {
       var uneCase = cases[i];
       uneCase.onclick = function() {
           if(this.className == 'case' && winner == 0) {
              play(this);
           }
       }
   }
}

init()
